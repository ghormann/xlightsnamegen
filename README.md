This is a python process to listen to the MQTT server of names, and current songs playing.  When it is notified on specific songs, it will use the sequence template to genereate a sequence to be loaded to the fpp.

## Pre-Reqs
* pip install paho-mqtt

## Run
1. source .env/bin/activate
1. python main.py

